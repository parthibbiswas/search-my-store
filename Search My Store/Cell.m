//
//  BrandCell.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import "Cell.h"

@implementation Cell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
