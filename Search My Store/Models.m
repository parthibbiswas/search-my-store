//
//  Models.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import "Models.h"
#import "Cell.h"
#import "Stores.h"

@interface Models ()

@end

@implementation Models

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData] ;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeBack:)] ;
    
    swipe.direction = UISwipeGestureRecognizerDirectionRight ;
    
    [self.view addGestureRecognizer:swipe] ;
    
}

-(IBAction)swipeBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(void)loadData{
    NSString *str_url = [NSString stringWithFormat:@"http://220.225.80.177/FindStore/webservice.asmx/Flavour_Search?brand_id=%@", self.modelURLid] ;
    
    NSURL *url = [NSURL URLWithString:str_url] ;
    
    NSData *data = [NSData dataWithContentsOfURL:url] ;
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] ;
    
    NSLog(@"%@", dict) ;
    
    NSString* suc=[dict objectForKey:@"success"];
    
    int success = [suc intValue] ;
    
    if (success == 1) {
        modelArray = [dict objectForKey:@"flavors"] ;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return modelArray.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Cell *cell = [modelTable dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath] ;
    
    cell.label.text = [[modelArray objectAtIndex:indexPath.row] objectForKey:@"flavor_name"];
    
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil] ;
    
    Stores *obj = [sb instantiateViewControllerWithIdentifier:@"stores"] ;
    
    obj.str_id = [[modelArray objectAtIndex:indexPath.row] objectForKey:@"id"] ;
    obj.str_brandid = [[modelArray objectAtIndex:indexPath.row] objectForKey:@"brand_id"] ;
    
    [self.navigationController pushViewController:obj animated:YES] ;
    
}

-(IBAction)backButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(IBAction)logoutButton:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
