//
//  Models.h
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Models : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    IBOutlet UITableView *modelTable ;
    
    NSArray *modelArray ;
}

@property (strong,nonatomic) NSString *modelURLid;

-(IBAction)backButton:(id)sender ;
-(IBAction)logoutButton:(id)sender ;
@end
