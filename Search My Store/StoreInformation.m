//
//  StoreInformation.m
//  Search My Store
//
//  Created by Parthib Biswas on 30/03/17.
//  Copyright © 2017 ejob. All rights reserved.
//

#import "StoreInformation.h"

@interface StoreInformation ()

@end

@implementation StoreInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    map.mapType = MKMapTypeStandard ;
    
    if ([self.nameText isEqualToString:@""]){
        [nameLabel setText:@"-"] ;
    } else {
        [nameLabel setText:self.nameText] ;
    }
    
    if ([self.addressText isEqualToString:@""]){
        [addressLabel setText:@"-"] ;
    } else {
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.numberOfLines = 0;
        [addressLabel setText:self.addressText] ;
        [addressLabel sizeToFit];
    }
    
    if ([self.zipText isEqualToString:@""]){
        [zipLabel setText:@"-"] ;
    } else {
        [zipLabel setText:self.zipText] ;
    }
    
    if ([self.cityText isEqualToString:@""]){
        [cityLabel setText:@"-"] ;
    } else {
        [cityLabel setText:self.cityText] ;
    }
    
    if ([self.countryText isEqualToString:@""]){
        [countryLabel setText:@"-"] ;
    } else {
        [countryLabel setText:self.countryText] ;
    }
    
    if ([self.stateText isEqualToString:@""]){
        [stateLabel setText:@"-"] ;
    } else {
        [stateLabel setText:self.stateText] ;
    }
    
    if ([self.phoneNumberText isEqualToString:@""]){
        [phoneNumberLabel setText:@"-"] ;
    } else {
        [phoneNumberLabel setText:self.phoneNumberText] ;
    }
    
    if ([self.websiteText isEqualToString:@""]){
        [websiteLabel setText:@"-"] ;
    } else {
        [websiteLabel setText:self.websiteText] ;
    }
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeBack:)] ;
    
    swipe.direction = UISwipeGestureRecognizerDirectionRight ;
    
    [self.view addGestureRecognizer:swipe] ;
    
    [self maplocation];
    
}

-(void)maplocation
{

    double  lat=[self.latitude doubleValue];
    double lon=[self.longitude doubleValue];
    
    CLLocationCoordinate2D zoomlocation;
    zoomlocation.latitude=lat;
    zoomlocation.longitude=lon;
    MKCoordinateRegion viewRegion= MKCoordinateRegionMakeWithDistance(zoomlocation, 1000, 1000);
    [map setRegion:viewRegion animated:YES];
    
    MKPointAnnotation *pinpoint=[[MKPointAnnotation alloc]init];
    pinpoint.coordinate= zoomlocation;
    pinpoint.title= _nameText;
    pinpoint.subtitle= [NSString stringWithFormat:@"Hi %.2f, %.2f",zoomlocation.latitude,zoomlocation.longitude];
    [map addAnnotation:pinpoint];

}

-(IBAction)swipeBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(IBAction)backButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(IBAction)logoutButton:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES] ;
}

-(void)viewDidLayoutSubviews{
    [scroll setContentSize:CGSizeMake(0, 750)] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
