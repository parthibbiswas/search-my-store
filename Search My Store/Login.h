//
//  ViewController.h
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Login : UIViewController{
    IBOutlet UITextField *username ;
    IBOutlet UITextField *password ;
}

-(IBAction)login:(id)sender ;
-(IBAction)info:(id)sender ;

@end

