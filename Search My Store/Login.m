//
//  ViewController.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import "Login.h"
#import "Brands.h"

@interface Login ()

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)] ;
    
    [self.view addGestureRecognizer:tap] ;
    
}

-(IBAction)closeKeyboard:(id)sender{
    [username resignFirstResponder] ;
    [password resignFirstResponder] ;
}

-(IBAction)login:(id)sender{
    if ([username.text  isEqualToString: @"aaa"] && [password.text  isEqualToString: @"aaa"]){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil] ;
        
        Brands *obj = [sb instantiateViewControllerWithIdentifier:@"brands"] ;
        
        [self.navigationController pushViewController:obj animated:YES] ;
    }
    else
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:nil message:@"Wrong user" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

-(IBAction)info:(id)sender{
    UIAlertView *info = [[UIAlertView alloc]initWithTitle:nil message:@"This app is created by Parthib Biswas.\n\n Info icon, Back button icon and Logout icon created by Freepik. Visit http://www.flaticon.com/authors/freepik to see more of his works." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
    
    [info show] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
