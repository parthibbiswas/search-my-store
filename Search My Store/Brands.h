//
//  Brands.h
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Brands : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    IBOutlet UITableView *brandTable ;
    
    NSArray *brandArray ;
}

-(IBAction)logoutButton:(id)sender ;

@end
