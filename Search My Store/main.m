//
//  main.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
