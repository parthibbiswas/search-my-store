//
//  Stores.h
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Stores : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    NSArray *storeArray ;
    
    IBOutlet UITableView *storeTable ;
}

@property (strong, nonatomic) NSString *str_id ;
@property (strong, nonatomic) NSString *str_brandid ;

-(IBAction)backButton:(id)sender ;
-(IBAction)logoutButton:(id)sender;

@end
