//
//  StoreInformation.h
//  Search My Store
//
//  Created by Parthib Biswas on 30/03/17.
//  Copyright © 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface StoreInformation : UIViewController{
    NSArray *storeArray ;
    
    IBOutlet UILabel *nameLabel ;
    IBOutlet UILabel *addressLabel ;
    IBOutlet UILabel *zipLabel ;
    IBOutlet UILabel *cityLabel ;
    IBOutlet UILabel *countryLabel ;
    IBOutlet UILabel *stateLabel ;
    IBOutlet UILabel *phoneNumberLabel ;
    IBOutlet UILabel *websiteLabel ;
    
    IBOutlet UIScrollView *scroll ;
    
    IBOutlet MKMapView *map ;
}

@property (strong, nonatomic) NSString *nameText ;
@property (strong, nonatomic) NSString *addressText ;
@property (strong, nonatomic) NSString *zipText ;
@property (strong, nonatomic) NSString *cityText ;
@property (strong, nonatomic) NSString *countryText ;
@property (strong, nonatomic) NSString *stateText ;
@property (strong, nonatomic) NSString *phoneNumberText ;
@property (strong, nonatomic) NSString *websiteText ;

@property (strong, nonatomic) NSString *str_id ;
@property (strong, nonatomic) NSString *str_brandid ;

@property (strong, nonatomic)  NSString *latitude;
@property (strong, nonatomic)  NSString *longitude;

-(IBAction)backButton:(id)sender ;
-(IBAction)logoutButton:(id)sender;

@end
