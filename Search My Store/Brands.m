//
//  Brands.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import "Brands.h"
#import "Cell.h"
#import "Models.h"

@interface Brands ()

@end

@implementation Brands

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData] ;
}

-(void)loadData{
    NSString *str_url = [NSString stringWithFormat:@"http://220.225.80.177/FindStore/webservice.asmx/Brand_Search"] ;
    
    NSURL *url = [NSURL URLWithString:str_url] ;
    
    NSData *data = [NSData dataWithContentsOfURL:url] ;
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] ;
    
    NSLog(@"%@", dict) ;
    
    NSString* suc=[dict objectForKey:@"success"];
    
    int success = [suc intValue] ;
    
    if (success == 1) {
        brandArray = [dict objectForKey:@"brand_details"] ;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return brandArray.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Cell *cell = [brandTable dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath] ;
    
    cell.label.text = [[brandArray objectAtIndex:indexPath.row] objectForKey:@"brand_name"] ;
    
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil] ;
    
    Models *obj = [sb instantiateViewControllerWithIdentifier:@"models"] ;
    
    obj.modelURLid = [[brandArray objectAtIndex:indexPath.row ]objectForKey:@"id"] ;
    
    [self.navigationController pushViewController:obj animated:YES] ;
}

-(IBAction)logoutButton:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
