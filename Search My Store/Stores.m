//
//  Stores.m
//  Search My Store
//
//  Created by Apple on 30/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import "Stores.h"
#import "Cell.h"
#import "StoreInformation.h"

@interface Stores ()

@end

@implementation Stores

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData] ;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeBack:)] ;
    
    swipe.direction = UISwipeGestureRecognizerDirectionRight ;
    
    [self.view addGestureRecognizer:swipe] ;
    
}

-(IBAction)swipeBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(IBAction)backButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES] ;
}

-(IBAction)logoutButton:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES] ;
}

-(void)loadData{
    NSString *str_url = [NSString stringWithFormat:@"http://220.225.80.177/FindStore/webservice.asmx/Store_Search?brand_id=%@&flavour_id=%@", self.str_brandid, self.str_id] ;
    
    NSURL *url = [NSURL URLWithString:str_url] ;
    
    NSData *data = [NSData dataWithContentsOfURL:url] ;
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] ;
    
    NSLog(@"%@", dict) ;
    
    NSString* suc=[dict objectForKey:@"success"];
    
    int success = [suc intValue] ;
    
    if (success == 1) {
        storeArray = [dict objectForKey:@"store_information"] ;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return storeArray.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Cell *cell = [storeTable dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath] ;
    
    cell.label.text = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"store_name"] ;
    
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil] ;
    
    StoreInformation *obj = [sb instantiateViewControllerWithIdentifier:@"information"] ;
    
    obj.nameText = [NSString stringWithFormat:@"%@", [[storeArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]] ;
    obj.addressText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"store_address"] ;
    obj.zipText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"zip"] ;
    obj.cityText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"city"] ;
    obj.countryText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"country"] ;
    obj.stateText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"state"] ;
    obj.phoneNumberText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"phone_number"] ;
    obj.websiteText = [[storeArray objectAtIndex:indexPath.row] objectForKey:@"store_url"] ;
    obj.latitude=[[storeArray objectAtIndex:indexPath.row] objectForKey:@"latitude"] ;
    obj.longitude=[[storeArray objectAtIndex:indexPath.row] objectForKey:@"longitude"] ;
    
    [self.navigationController pushViewController:obj animated:YES] ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
